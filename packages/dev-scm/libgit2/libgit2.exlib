# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] cmake [ api=2 ]

SUMMARY="C implementation of the Git core methods as a library with a solid API"
DESCRIPTION="LibGit2 is a from the ground up implementation of a Git library."
HOMEPAGE+=" https://libgit2.github.com"

LICENCES="GPL-2 [[ note = [ With linking exception ] ]]"
SLOT="0"
MYOPTIONS="
    kerberos [[ description = [ Support for SPNEGO authentication ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

# Tests needs access to internet and fails playing with git inside build tree
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-libs/http-parser[>=2.0.0]
        net-libs/libssh2
        net-misc/curl
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_CLAR=:BOOL=FALSE # we don't run the tests
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCURL:BOOL=TRUE
    -DCMAKE_BUILD_TYPE:STRING="Release"
    -DDEBUG_POOL:BOOL=FALSE
    -DENABLE_REPRODUCIBLE_BUILDS:BOOL=FALSE
    -DENABLE_TRACE:BOOL=FALSE
    -DENABLE_WERROR:BOOL=FALSE
    -DPROFILE:BOOL=FALSE
    -DSHA1_BACKEND:STRING="CollisionDetection"
    -DTAGS:BOOL=FALSE
    -DTHREADSAFE:BOOL=TRUE
    -DUSE_BUNDLED_ZLIB:BOOL=FALSE
    -DUSE_EXT_HTTP_PARSER:BOOL=TRUE
    -DUSE_HTTPS:STRING="OpenSSL"
    -DUSE_NSEC:BOOL=TRUE
    -DUSE_SSH:BOOL=TRUE
    -DVALGRIND:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_USES=( 'kerberos GSSAPI' )

