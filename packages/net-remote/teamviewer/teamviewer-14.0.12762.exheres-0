# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ opt/teamviewer/tv_bin/script/teamviewerd.service ] ] \
    gtk-icon-cache

SUMMARY="Remote control and meeting solution"
DESCRIPTION="
TeamViewer provides easy, fast and secure remote access and meeting solutions to Linux, Windows
PCs, Apple PCs and various other platforms, including Android and iPhone. TeamViewer is free for
personal use. You can use TeamViewer completely free of charge to access your private computers or
to help your friends with their computer problems.
"
HOMEPAGE="https://www.teamviewer.com"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://dl.tvcdn.de/download/linux/version_$(ever major)x/${PN}_${PV}_amd64.deb )
"

LICENCES="
    AFL-2.1 [[ note = [ D-Bus component ] ]]
    BSD-3 [[ note = [ Google Coredumper component ] ]]
    BSD-4 [[ note = [ SSLeay component ] ]]
    TeamViewer
    openssl [[ note = [ OpenSSL component ] ]]
"
SLOT="0"
PLATFORMS="-* ~amd64"
MYOPTIONS="
    platform: amd64
"

QT5_MIN_VER="5.5.1"

RESTRICT="strip"

DEPENDENCIES="
    run:
        sys-apps/dbus
        x11-apps/xdg-utils
        x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT5_MIN_VER}]
        x11-libs/qtwebkit:5[>=${QT5_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT5_MIN_VER}]
    suggestion:
        fonts/liberation-fonts [[ description = [ Recommended font ] ]]
"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    default
    edo tar xf data.tar.xz
}

src_prepare() {
    edo sed \
        -e 's:"$(dirname "$(readlink -e "$0")")":/opt/teamviewer/tv_bin/script:g' \
        -i opt/teamviewer/tv_bin/script/teamviewer
}

src_test() {
    :
}

src_install() {
    doins -r opt

    insinto /usr
    doins -r usr/share

    exeinto /usr/$(exhost --target)/bin
    doexe opt/teamviewer/tv_bin/script/${PN}

    keepdir /etc/${PN}
    keepdir /var/log/${PN}$(ever major)

    install_systemd_files

    edo chmod 0755 "${IMAGE}"/opt/teamviewer/tv_bin/{TeamViewer,teamviewer-config,teamviewerd,TeamViewer_Desktop}
    edo chmod 0755 "${IMAGE}"/opt/teamviewer/tv_bin/script/${PN}

    edo rm "${IMAGE}"/opt/teamviewer/tv_bin/script/{libdepend,teamviewerd.sysv,teamviewer_setup,tv-delayed-start.sh}
    edo rm -r "${IMAGE}"/opt/teamviewer/tv_bin/xdg-utils
}

