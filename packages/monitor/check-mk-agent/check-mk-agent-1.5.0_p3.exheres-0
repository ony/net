# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${PV/_}"
MY_PNV="check-mk-raw-${MY_PV}.cre"

require systemd-service [ systemd_files=[ agents/cfg_examples/systemd/check_mk{.socket,@.service} ] ]

SUMMARY="Check_MK Agent for Linux"
DESCRIPTION="
The Check_MK Agent uses xinetd to provide information about the system on TCP port 6556. This can
be used to monitor the host via Check_MK.
"
HOMEPAGE="https://mathias-kettner.de/check_mk.html"
DOWNLOADS="https://mathias-kettner.de/support/${MY_PV}/${MY_PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="xinetd"

DEPENDENCIES="
    run:
        xinetd? ( sys-apps/xinetd )
    recommendation:
        dev-lang/python:*[<3] [[
            description = [ Required by some plugins written in Python ]
        ]]
"

WORK=${WORKBASE}/${MY_PNV}

src_prepare() {
    # Errors when running under Python 3
    edo sed \
        -e 's:/usr/bin/python:/usr/bin/python2:' \
        -i agents/plugins/{apache_status,isc_dhcpd,mk_inotify,mk_mongodb}

    default
}

src_configure() {
    :
}

src_compile() {
    :
}

src_test() {
    :
}

src_install() {
    newbin agents/check_mk_agent.linux check_mk_agent
    newbin agents/check_mk_caching_agent.linux check_mk_caching_agent
    dobin agents/mk-job

    keepdir /etc/check_mk
    keepdir /usr/$(exhost --target)/lib/check_mk_agent/{,plugins,local,job,spool}

    install_systemd_files

    if option xinetd ; then
        insinto /etc/xinetd.d
        newins agents/cfg_examples/xinetd.conf check_mk
    fi

    # Think about making the installation of the plugins configurable via SUBOPTIONS
    exeinto /usr/$(exhost --target)/lib/check_mk_agent/plugins
    doexe agents/plugins/{apache_status,isc_dhcpd,lnx_quota,lvm,mk_filehandler,mk_inotify,mk_inventory.linux,mk_logins,netstat.linux,nfsexports,smart}
    doexe agents/plugins/{mk_mongodb,mk_mysql,mk_oracle,mk_postgres}

    insinto /etc/check_mk
    doins agents/cfg_examples/*.cfg
}

