# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=vstakhov tag=${PV} ] cmake [ api=2 ]

export_exlib_phases src_prepare src_configure src_install

SUMMARY="Fast, free and open-source spam filtering system"
DESCRIPTION="
Rspamd is an advanced spam filtering system that allows evaluation of messages
by a number of rules including regular expressions, statistical analysis and
custom services such as URL black lists. Each message is analysed by Rspamd
and given a spam score.
According to this spam score and the user’s settings Rspamd recommends an
action for the MTA to apply to the message: for example, to pass, to reject or
to add a header. Rspamd is designed to process hundreds of messages per second
simultaneously and has a number of features available.
"

HOMEPAGE="https://rspamd.com/"

LICENCES="
    Apache-2.0
    BSD-3 [[ note = [ contrib/snowball ] ]]
"
SLOT="0"
MYOPTIONS="
    examples
    image-processing [[ description = [ Enable libgd for images processing ] ]]
    jemalloc
    luajit           [[ description = [ Prefer LuaJIT over plain lua ] ]]
    webui            [[ description = [ Install a web interface ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-util/ragel
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2
        dev-libs/icu:=
        dev-libs/libevent:=
        dev-libs/pcre2
        group/rspamd
        user/rspamd
        sys-apps/file   [[ note = [ libmagic ] ]]
        sys-libs/zlib
        image-processing? ( media-libs/gd )
        jemalloc? ( dev-libs/jemalloc )
        luajit? ( dev-lang/LuaJIT )
       !luajit? ( dev-lang/lua:5.3 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    recommendation:
        dev-db/redis [[ description = [
            ${PN}'s default data storage and caching system
        ] ]]
    suggestion:
        net-dns/unbound [[ description = [
            ${PN} does a lot of DNS requests, so caching might make sense
        ] ]]

"
# TODO: Bundles hiredis, snowball, uthash, zstd, lpeg amongst other things

BUGS_TO="heirecka@exherbo.org"

REMOTE_IDS="freshcode:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}doc/index.html"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}changes.html"

# I realize cmake variables aren't strongly typed, but hey, why interpret
# them as bool when you can do string matching!
_cmake_str_option() {
    [[ -n ${2} ]] || die "_cmake_str_enable <option flag> [<flag name>]"
    local flag
    if [[ -n ${3} ]]; then
        flag=${2}
    else
        flag=${2,,}
    fi
    echo "-D${1:+${1}_}${3:-$(optionfmt ${2})}:STRING=$(option "${flag}" && echo ON || echo OFF)"
}

rspamd_src_prepare() {
    cmake_src_prepare

    edo sed -e "/User=/ s/_rspamd/rspamd/" -i ${PN}.service
}

rspamd_src_configure() {
    local cmakeparams=(
        -DCONFDIR="/etc/${PN}"
        -DMANDIR="/usr/share/man"
        -DEXAMPLESDIR="/usr/share/${PN}/examples"
        -DPLUGINSDIR="/usr/share/${PN}"
        -DWWWDIR="/usr/share/${PN}/www"
        -DWANT_SYSTEMD_UNITS:STRING=ON
        # "Enable clang static analysing plugin"
        -DENABLE_CLANG_PLUGIN:STRING=OFF
        # "Enable fann for neural network plugin"
        -DENABLE_FANN:STRING=OFF
        # "Enable google perftools"
        -DENABLE_GPERF_TOOLS:STRING=OFF
        # "Enable hyperscan for fast regexp processing" - only available on x86_64
        -DENABLE_HYPERSCAN:BOOL=FALSE
        -DENABLE_PCRE2:STRING=ON
        # Snowball stemming system, bundled
        -DENABLE_SNOWBALL:STRING=ON
        # Needs luajit, it's a machine learning lib, bundled
        -DENABLE_TORCH:STRING=OFF
        $(_cmake_str_option '' examples INSTALL_EXAMPLES)
        $(_cmake_str_option ENABLE image-processing GD)
        $(_cmake_str_option ENABLE JEMALLOC)
        $(_cmake_str_option ENABLE luajit LUAJIT)
        $(_cmake_str_option '' webui INSTALL_WEBUI)
    )

    ecmake "${cmakeparams[@]}"
}

rspamd_src_install() {
    default

    option examples || edo rmdir "${IMAGE}"/usr/share/${PN}/examples

    keepdir /var/lib/${PN}
    keepdir /var/log/${PN}
    edo chown -R rspamd:rspamd "${IMAGE}"/var/{lib,log}/${PN}
}

