# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'privoxy-3.0.10.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ none ] ]
require systemd-service

SUMMARY="Privacy-focused web proxy with advanced filtering capabilities"
DESCRIPTION="
Privoxy is a non-caching web proxy with advanced filtering capabilities for
enhancing privacy, modifying web page data, managing HTTP cookies, controlling
access, and removing ads, banners, pop-ups and other obnoxious Internet junk.
Privoxy has a flexible configuration and can be customized to suit individual
needs and tastes. Privoxy has application for both stand-alone systems and
multi-user networks. Privoxy is based on Internet Junkbuster.
"
HOMEPAGE="http://www.privoxy.org/"
DOWNLOADS="mirror://sourceforge/ijbswa/${PNV}-stable-src.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        dev-libs/pcre

        group/privoxy
        user/privoxy

    suggestion:
        app-admin/logrotate [[ description = [ Use logrotate for rotating logs ] ]]
"

BUGS_TO="alip@exherbo.org"
REMOTE_IDS="freecode:${PN} sourceforge:ijbswa"
UPSTREAM_DOCUMENTATION="http://www.privoxy.org/user-manual/index.html [[ lang = en ]]"

WORK=${WORKBASE}/${PNV}-stable

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --sysconfdir=/etc/privoxy
    --enable-dynamic-pcre
    --enable-large-file-support
    --enable-zlib
    --with-group=privoxy
    --with-user=privoxy
)

src_prepare() {
    default

    # Sane defaults for /etc/privoxy/config
    edo sed \
        -e 's:confdir .:confdir /etc/privoxy:' \
        -e 's:logdir .:logdir /var/log/privoxy:' \
        -e 's:logfile logfile:logfile privoxy.log:' \
        -i config

    if option doc; then
        edo sed \
            -e "s~^#user-manual .*~user-manual /usr/share/doc/${PNVR}/html/user-manual~" \
            -i config
    fi

    # autoheader and autoconf need to be called even if we don't modify any autotools
    # source files. See main Makefile.
    eautoheader
    eautoconf
}

src_install() {
    dobin privoxy
    install_systemd_files

    insinto /etc/logrotate.d
    newins "${FILES}"/privoxy.logrotate privoxy

    insinto /etc/privoxy
    doins config \
          default.action \
          default.filter \
          match-all.action \
          trust \
          user.action \
          user.filter

    insinto /etc/privoxy/templates
    doins templates/*

    doman privoxy.1
    emagicdocs
    if option doc; then
        docinto html
        dodoc -r doc/webserver/*
    fi

    diropts -m 0750 -g privoxy -o privoxy
    keepdir /var/log/privoxy
}

