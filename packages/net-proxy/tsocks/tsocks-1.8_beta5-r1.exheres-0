# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'tsocks-1.8_beta5-r3.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.10 ] ] multibuild

SUMMARY="Transparent SOCKS proxying library"
DESCRIPTION="
tsocks is a wrapper library for the libc connect() call, providing transparent
network access through a SOCKS v4 or v5 proxy (usually on a firewall). tsocks
intercepts the calls applications make to create TCP connections and determines
if the destinations can be directly accessed or if the SOCKS server is needed.
In the latter case a connection is transparently negotiated with the SOCKS
server. This allows existing applications to use SOCKS without recompilation or
modification.
"
HOMEPAGE="http://tsocks.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/tsocks/${PNV/_}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

REMOTE_IDS="freecode:${PN} sourceforge:${PN}"

WORK="${WORKBASE}"/${PNV%%_*}

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-gentoo-r1.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --libdir=/usr/$(exhost --target)/lib
    --with-conf=/etc/socks/tsocks.conf
)

DEFAULT_SRC_COMPILE_PARAMS=( "DYNLIB_FLAGS=-Wl,--soname,libtsocks.so.${PV/_beta*}" )

src_install() {
    default

    dobin inspectsocks
    newbin saveme tsocks-saveme
    newbin validateconf tsocks-validateconf

    insinto /etc/socks
    doins tsocks.conf.*.example
}

pkg_postinst() {
    elog "Before using tsocks you need a configuration in /etc/socks/tsocks.conf."
    elog "Examples can be found in /etc/socks/tsocks.conf.*.example."
    elog
    elog "The following executables have been renamed:"
    elog "    /usr/$(exhost --target)/bin/saveme -> /usr/bin/tsocks-saveme"
    elog "    /usr/$(exhost --target)/bin/validateconf -> /usr/bin/tsocks-validateconf"
}

